"""
    Common utilities
"""

import os
import subprocess

from utils.config_util import get_config


def is_odroid():
    """
    Check if current system is Odroid or not
    :return: 
    """
    return os.uname()[4] == 'aarch64'


def get_id():
    if is_odroid():
        pipe = os.popen('cat /sys/class/efuse/usid')
        data = pipe.read().strip()
        pipe.close()
    else:
        data = 'HKC213254DFE4330'

    return data


def get_video_source():
    return get_config('video', 'video_source', 'picamera')


def is_picamera():
    return is_odroid() and get_video_source() == 'picamera'


def is_running_on_ssh():
    return 'SSH_CLIENT' in os.environ or 'SSH_TTY' in os.environ


def is_debug():
    return get_config('face', 'DEBUG', 'False').lower() == 'true'


def allow_upload():
    return get_config('face', 'UPLOAD', 'True').lower() == 'true'


def get_remote_git_url():
    pipe = os.popen("cd {}; cat .git/config | grep url")
    data = pipe.read().strip()
    pipe.close()
    if data.startswith('url = '):
        url = data.split()[-1]
        return url


def get_image_file_to_save():
    return '/var/www/html/{}.png'.format(get_id())


def check_git_commit():
    """
    Compare local/remote commit datetime, and pull if any updates in the remote repo.
    :return:
    """
    # pipe = os.popen("cd {}; git for-each-ref "
    #                 "--format='%(committerdate:raw) %(refname)' --sort -committerdate".format(os.getcwd()))
    """ Sample output:
        1499902068 +0200 refs/heads/develop
        1499902068 +0200 refs/remotes/origin/develop
        1499758420 +0100 refs/remotes/origin/HEAD
        1499758420 +0100 refs/remotes/origin/master
        1498788881 +0100 refs/heads/master
    """
    cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
    p = subprocess.Popen('/bin/bash {}/git_update.sh'.format(cur_dir), shell=True,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = p.communicate()
    p.wait()
    print stdout, stderr

    return stdout


if __name__ == '__main__':

    print check_git_commit()

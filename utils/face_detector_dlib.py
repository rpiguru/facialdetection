import copy
import glob
import cv2
import datetime
import dlib


dlib_detector = dlib.get_frontal_face_detector()

dlib_up_sample = 1


def detect_faces(frame):
    """
    Detect faces in the current frame
    :param frame:
    :return:
    """
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    face_rects = dlib_detector(gray_frame, dlib_up_sample)
    tmp_frame = copy.deepcopy(frame)
    rects = []
    if len(face_rects) > 0:
        for k, r in enumerate(face_rects):
            if r.left() < 0 or r.right() < 0 or r.top() < 0 or r.bottom() < 0:
                continue
            cv2.rectangle(tmp_frame, (r.left(), r.top()), (r.right(), r.bottom()), (0, 255, 0), 1)
            print datetime.datetime.now(), '   Face found, ({}, {}) - ({}, {}),  {}x{}'.format(
                r.left(), r.top(), r.right(), r.bottom(), r.right() - r.left(), r.bottom() - r.top())
            rects.append(r)

    if len(rects) > 0:
        print 'Total faces: {}'.format(len(rects))

    return rects


if __name__ == '__main__':

    for img in glob.glob('../faces/*.png'):
        faces = detect_faces(cv2.imread(img))
        print '{}: {}'.format(img, faces)

# Facial Detection
==============================================================

## 1. Components

- Odroid C2

- USB Webcam

- Preparing Odroid
    
    **Image file:** https://my.pcloud.com/publink/show?code=XZymf77Zab2PUgIhCV0ziL7IqmFBbBFTEgky
    
    Download Ubuntu Mate 16.04
        
        http://de.eu.odroid.in/ubuntu_16.04lts/ubuntu64-16.04.2lts-mate-odroid-c2-20170301.img.xz
    
    Unzip this file and write .img file to the 16G Micro SD card.
    
    (NOTE: 8G won't work, it is not enough... )

- Enable Auto-login
    
    Edit `/usr/share/lightdm/lightdm.conf.d/60-lightdm-gtk-greeter.conf` file like this.
    
        [Seat:*]
        greeter-session=lightdm-gtk-greeter
        autologin-user=odroid 

- GPU acceleration should be enabled manually. Add below line into `/etc/chromium-browser/default` file.
    
        CHROMIUM_FLAGS=" --use-gl=egl --ignore-gpu-blacklist --disable-accelerated-2d-canvas --num-raster-threads=2"

## 2. Preparing

1) Get unique ID of Odroid

    SSH into the Odroid and type this:

        odroid@odroid64:/$ cat /sys/class/efuse/usid
        HKC213254DFEACDF
        odroid@odroid64:/$

    The Unique ID of Odroid is `HKC213254DFEACDF`.

2) Using pre-installed image file.

    After flashing our customized .img file, ssh into the Odroid, and type this:

        cd ~/facialdetection
        git pull

    Now, reboot with `sudo reboot` and that's all!

3) Add **write** permission to `/var/www/html`

        sudo chmod o+w /var/www/html

4) Connecting to a WiFi AP:

        sudo nmcli dev wifi connect "<SSID>" password "<Password>"

5) Disconnecting:

        sudo nmcli d disconnect wlan0


6) For mass production (Admin only)

        sudo apt-get install curl
        sudo cp conf/mass-install-dp /etc/network/if-up.d/
        sudo chmod 755 /etc/network/if-up.d/mass-install-dp
        sudo rm /opt/dataplicity/mass-install-hostname
        sudo rm /opt/dataplicity/tuxtunnel/auth


## 3. Install Packages

1) Download source files
    
        sudo apt-get update
        sudo apt-get install -y git
        git clone https://github.com/reaconads/facialdetection

    Once cloned the source codes, need to setup ssh pubkey to enable pulling the source code automatically.

        mkdir ~/.ssh
        eval "$(ssh-agent -s)"
        cd facialdetection/cert
        chmod 400 deploy_ro_1*
        ssh-add deploy_ro_1

    Open the `.git/config` file and change `url = https://github.com/reaconads/facialdetection.git` to `url = git@github.com:reaconads/facialdetection.git`


2) Install dependencies
  
        cd utils
        sudo chmod +x pre_install.sh
        sudo ./pre_install.sh

3) Install dlib
        
        cd ~
        git clone https://github.com/davisking/dlib
        cd dlib
    
    Modify something:
    
    Open the `dlib/image_processing/object_detector.h` and go to the **line 256** and add a line like this:
    
    Original:
        
        deserialize(num_detectors, in);
        item.w.resize(num_detectors);
        for (unsigned long i = 0; i < item.w.size(); ++i)
        {
            deserialize(item.w[i].w, in);
            item.w[i].init(item.scanner);
        }
    
    Add `num_detectors = 3;`:
    
        deserialize(num_detectors, in);
        num_detectors = 3;                      << Added this line
        item.w.resize(num_detectors);
        for (unsigned long i = 0; i < item.w.size(); ++i)
        {
            deserialize(item.w[i].w, in);
            item.w[i].init(item.scanner);
        }
    
    Now, build **dlib**

        sudo python setup.py install --yes USE_AVX_INSTRUCTIONS --compiler-flags "-O3"

4) Install caffe on Odroid

- Install dependencies

        sudo apt-get install -y libprotobuf-dev libleveldb-dev libsnappy-dev libhdf5-serial-dev protobuf-compiler
        sudo apt-get install -y libatlas-base-dev
        sudo apt-get install -y --no-install-recommends libboost-all-dev
        sudo apt-get install -y libgflags-dev libgoogle-glog-dev liblmdb-dev
        sudo apt-get install -y python-dev
        sudo apt-get install -y python-numpy python-scipy

- Download source code

        cd ~
        wget https://github.com/BVLC/caffe/archive/rc5.zip
        unzip rc5.zip
        cd caffe-rc5
        cp Makefile.config.example Makefile.config

- Open the `Makefile.config` with `nano Makefile.config` and change something like this:

        CPU_ONLY := 1

        OPENCV_VERSION := 3

        PYTHON_INCLUDE := /usr/include/python2.7 /usr/lib/python2.7/dist-packages/numpy/core/include

        WITH_PYTHON_LAYER := 1

        INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include /usr/include/hdf5/serial

        LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib /usr/lib/aarch64-linux-gnu /usr/lib/aarch64-linux-gnu/hdf5/serial

- Now, execute this:

        find . -type f -exec sed -i -e 's^"hdf5.h"^"hdf5/serial/hdf5.h"^g' -e 's^"hdf5_hl.h"^"hdf5/serial/hdf5_hl.h"^g' '{}' \;

        cd /usr/lib/aarch64-linux-gnu

        sudo ln -s libhdf5_serial.so.10.1.0 libhdf5.so

        sudo ln -s libhdf5_serial_hl.so.10.0.2 libhdf5_hl.so

- Open `Makefile` and change

        NVCCFLAGS += -ccbin=$(CXX) -Xcompiler -fPIC $(COMMON_FLAGS)

    to:

        NVCCFLAGS += -D_FORCE_INLINES -ccbin=$(CXX) -Xcompiler -fPIC $(COMMON_FLAGS)

    Change like this:

        LIBRARIES += glog gflags protobuf leveldb snappy \
        lmdb boost_system boost_filesystem hdf5_hl hdf5 m \
        opencv_core opencv_highgui opencv_imgproc opencv_imgcodecs opencv_videoio

- Also, open the file `CMakeLists.txt` and add the following line:

        # ---[ Includes
        set(${CMAKE_CXX_FLAGS} "-D_FORCE_INLINES ${CMAKE_CXX_FLAGS}")

- Install `leveldb` python package

        cd ~
        wget https://pypi.python.org/packages/03/98/1521e7274cfbcc678e9640e242a62cbcd18743f9c5761179da165c940eac/leveldb-0.20.tar.gz
        tar zvxf leveldb-0.20.tar.gz
        cd leveldb-0.20/leveldb/port
        rm atomic_pointer.h
        wget https://raw.githubusercontent.com/google/leveldb/c4c38f9c1f3bb405fe22a79c5611438f91208d09/port/atomic_pointer.h
        cd ~/leveldb-0.20
        sudo python setup.py install

- Install `h5py` python package

        cd ~
        sudo pip install cython
        wget https://pypi.python.org/packages/11/6b/32cee6f59e7a03ab7c60bb250caff63e2d20c33ebca47cf8c28f6a2d085c/h5py-2.7.0.tar.gz
        tar zvxf h5py-2.7.0.tar.gz
        cd h5py-2.7.0
        sudo python setup.py install


- Install python packages

        sudo pip install matplotlib ipython==5.0 networkx nose pandas protobuf python-gflags pyyaml Pillow scikit-image

- Build

        make all
        make pycaffe
        make distribute

- Add python path

        nano ~/.bashrc

    And add this:

        export PYTHONPATH=/home/odroid/caffe-rc5/python:$PYTHONPATH

    And apply this change with this:

        source ~/.bashrc

- Change something:

    Open `/home/odroid/caffe-rc5/python/caffe/io.py`, go to line 253-254 and replace

        if ms != self.inputs[in_][1:]:
            raise ValueError('Mean shape incompatible with input shape.')
    By

        if ms != self.inputs[in_][1:]:
            print(self.inputs[in_])
            in_shape = self.inputs[in_][1:]
            m_min, m_max = mean.min(), mean.max()
            normal_mean = (mean - m_min) / (m_max - m_min)
            mean = resize_image(normal_mean.transpose((1,2,0)),in_shape[1:]).transpose((2,0,1)) * (m_max - m_min) + m_min
            #raise ValueError('Mean shape incompatible with input shape.')


## 4. Install service

        sudo apt-get install supervisor

        sudo cp conf/face_detector.conf /etc/supervisor/conf.d/
        sudo service supervisor restart

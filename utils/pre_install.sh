#!/usr/bin/env bash

echo "Installing OpenCV 3.2.0"
sudo apt-get update

cd /home/odroid
echo "Removing any pre-installed ffmpeg and x264"
sudo apt-get remove -y ffmpeg x264 libx264-dev

echo "Installing Dependenices"
sudo apt-get install -y libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine2-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev libgtk2.0-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils ffmpeg cmake qt5-default checkinstall
sudo apt-get install -y libopenblas-dev liblapack-dev liblapacke-dev

echo "Downloading OpenCV 3.2.0"
wget -O OpenCV-3.2.0.zip http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/3.2.0/opencv-3.2.0.zip/download

echo "Installing OpenCV 3.2.0"
unzip OpenCV-3.2.0.zip
rm OpenCV-3.2.0.zip
cd opencv-3.2.0
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D WITH_LIBV4L=ON -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=OFF -D BUILD_EXAMPLES=OFF -D WITH_QT=OFF -D WITH_OPENGL=ON ..
make -j4
sudo make install
sudo sh -c 'echo "/usr/local/lib" > /etc/ld.so.conf.d/opencv.conf'
sudo ldconfig
cd /home/odroid
sudo rm -r opencv-3.2.0
echo "OpenCV" 3.2.0 "ready to be used"

echo "Installing dlib prerequisites"
sudo apt-get install -y build-essential cmake
sudo apt-get install -y libgtk-3-dev
sudo apt-get install -y libboost-all-dev
sudo apt-get install python-pip python-setuptools
sudo pip install --upgrade pip

sudo pip install scikit-image
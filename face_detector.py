#!/usr/bin/env python
import copy
import datetime
import logging.config
import multiprocessing
import os
import shutil
import threading
import time
from multiprocessing import Queue
import subprocess
import cv2
import dlib
import numpy
import requests

from utils.age_detector import detect_age
from utils.common import is_picamera, is_running_on_ssh, get_video_source, is_odroid, is_debug, get_id, \
    check_git_commit, allow_upload, get_image_file_to_save
from utils.config_util import get_config, set_config
from utils.gender_detector import detect_gender
from utils.video import get_resolution, get_frame_rate, clahe, save_to_file, compare_2_images

UPLOAD = False

cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

face_dir = cur_dir + 'faces'
if not os.path.exists(cur_dir + 'faces'):
    os.mkdir(cur_dir + 'faces')

logging.config.fileConfig(cur_dir + "logging.ini")
logger = logging.getLogger("Face")

dlib_detector = dlib.get_frontal_face_detector()

dlib_shape_predictor = dlib.shape_predictor(cur_dir + 'model/dlib/shape_predictor_68_face_landmarks.dat')

dlib_facerec = dlib.face_recognition_model_v1(cur_dir + 'model/dlib/dlib_face_recognition_resnet_model_v1.dat')

max_tracker_cnt = int(get_config('face', 'MAX_FACT_CNT', 3))
logger.debug('Max Face count: {}'.format(max_tracker_cnt))

dlib_up_sample = int(get_config('face', 'UP_SAMPLE', 1))
logger.debug('Dlib up-sampling: {}'.format(dlib_up_sample))

crop_face_size = int(get_config('face', 'CROP_FACE_SIZE', 60))
logger.debug('Crop face size: {}'.format(crop_face_size))

keep_dead = int(get_config('face', 'KEEP_DEAD', 5))
logger.debug('Keep time: {}'.format(keep_dead))

capture_interval = int(get_config('video', 'capture_interval', 10))
logger.debug('Capturing interval: {}'.format(capture_interval))


def detect_faces(frame):
    """
    Detect faces in the current frame
    :param frame:
    :return:
    """
    gray_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    face_rects = dlib_detector(gray_frame, dlib_up_sample)
    tmp_frame = copy.deepcopy(frame)
    rects = []
    if len(face_rects) > 0:
        for k, r in enumerate(face_rects):
            if r.left() < 0 or r.right() < 0 or r.top() < 0 or r.bottom() < 0:
                continue
            cv2.rectangle(tmp_frame, (r.left(), r.top()), (r.right(), r.bottom()), (0, 255, 0), 1)
            print datetime.datetime.now(), '   Face found, ({}, {}) - ({}, {}),  {}x{}'.format(
                r.left(), r.top(), r.right(), r.bottom(), r.right() - r.left(), r.bottom() - r.top())
            rects.append(r)

    if len(rects) > 0:
        print 'Total faces: {}'.format(len(rects))

    if not is_running_on_ssh() and not is_odroid():
        try:
            cv2.imshow('Faces', tmp_frame)
        except:
            pass

    return rects


def compare_rects(src_frame, src_rect, dst_frame, dst_rect):
    """
    Compare position of 2 rectangles and return True if both are similar
    :param src_frame: Source frame
    :param src_rect:  Source rectangle
    :param dst_frame: Destination frame
    :param dst_rect:  Destination rectangle
    :return:    True if these are same face, and vice versa
    """
    def get_desc_vector(_frame, rect):
        if rect is None:
            width, height = src_frame.shape[1::-1]
            rect = dlib.rectangle(left=0, top=0, right=width, bottom=height)
        shape = dlib_shape_predictor(_frame, rect)
        face_descriptor = dlib_facerec.compute_face_descriptor(_frame, shape)
        desc_vect = [float(v) for v in str(face_descriptor).splitlines()]
        return desc_vect
    if src_frame is None:
        src_frame = dst_frame
    desc1 = get_desc_vector(src_frame, src_rect)
    desc2 = get_desc_vector(dst_frame, dst_rect)
    offset = [desc1[i] - desc2[i] for i in range(len(desc1))]

    return True if sum(offset) < .6 else False


def find_similar_face(src_frame, src_rect, dst_frame, dst_faces):
    """
    Find a similar face in the list of faces
    :param src_frame: Source frame
    :param src_rect:  Source rect
    :param dst_frame: Destination frame
    :param dst_faces: Destination faces(rectangles)
    :return:
    """
    for r in dst_faces:
        if compare_rects(src_frame=src_frame, src_rect=src_rect, dst_frame=dst_frame, dst_rect=r):
            return r


def get_size_of_rect(r):
    return (r.right() - r.left()) * (r.bottom() - r.top())


def get_extended_face(frame, s):
    """
    Get extended face frame to be used in age/gender detector
    :param frame:
    :param s:
    :return:
    """
    factor = .1      # horizontal factor

    img_height, img_width, channels = frame.shape
    height = abs(s.right() - s.left())
    width = abs(s.bottom() - s.top())
    top = max(s.top() - height * factor * 4, 0)
    bottom = min(s.bottom(), img_height)
    left = max(s.left() - width * factor, 0)
    right = min(s.right() + width * factor, img_width)
    return frame[int(top):int(bottom), int(left):int(right)]


class FaceDetector:
    skip_factor = 1

    trackers = []
    last_track_time = time.time()

    b_stop = False
    q = None
    capture_time = 0
    cap = None

    def __init__(self, q=None):
        self.q = q
        self.skip_factor = int(get_config('face', 'SKIP_FACTOR', 1))
        logger.debug('Skip Factor: {}'.format(self.skip_factor))

        for i in range(max_tracker_cnt):
            tr = dict(enabled=False)
            self.trackers.append(tr)

    def start(self):
        self._start_normal()
        # if is_picamera():
        #     self._start_picamera()
        # else:
        #     self._start_normal()

    def stop(self):
        self.b_stop = True
        try:
            self.cap.release()
        except Exception as e:
            logger.exception('Failed to release camera... {}'.format(e))

    def _start_normal(self):
        if get_video_source() == 'sample':
            source = get_config('video', 'sample_video', 'sample/sample.mp4')
            logger.debug('Starting detector with sample video file: {}'.format(source))
            self.cap = cv2.VideoCapture(source)
        else:
            logger.debug('Starting detector with webcam...')
            self.cap = cv2.VideoCapture(0)

        width, height = get_resolution()
        logger.debug('Resolution: {}'.format(get_resolution()))
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

        ret, first_frame = self.cap.read()
        if first_frame is None:
            logger.error('Error: There is no camera connected on or cannot read the sample file!')
            restart_me()
            return False

        cnt = 0

        while self.cap.isOpened():
            # Skip the frames for speeding up
            cnt += 1
            if cnt % self.skip_factor != 0:
                continue

            # Read the frame one by one
            ret, frame = self.cap.read()
            if frame is None:
                logger.error('Error: Camera is disconnected.')
                break

            # FIXME: Need a flag for histogram equalization?
            resized_frame = cv2.resize(frame, (width, height))
            equalized_frame = clahe(resized_frame)

            if time.time() - self.capture_time > capture_interval:
                save_to_file(frame)
                self.capture_time = time.time()

            self._track_face(equalized_frame, resized_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):  # quit when 'q' keyword is pressed
                break

            if self.b_stop:
                break

        self.cap.release()
        cv2.destroyAllWindows()

    def _start_picamera(self):
        try:
            from picamera.array import PiRGBArray
            from picamera import PiCamera
        except ImportError:
            logger.error('`picamera module is not installed, please install')
            return
        logger.debug('Starting detector with picamera...')
        logger.debug('Resolution: {}'.format(get_resolution()))
        # Initialize the camera and grab a reference to the raw camera capture
        camera = PiCamera()
        camera.resolution = get_resolution()
        camera.framerate = get_frame_rate()
        raw_capture = PiRGBArray(camera, get_resolution())
        # Allow the camera to warm up
        time.sleep(.1)

        cnt = 0

        # Capture frames from the picamera
        for img in camera.capture_continuous(raw_capture, format='bgr', use_video_port=True):
            # Skip the frames for speeding up
            cnt += 1
            if cnt % self.skip_factor != 0:
                raw_capture.truncate(0)
                continue

            colored_frame = cv2.resize(numpy.asarray(img.array), get_resolution())
            equalized_frame = clahe(colored_frame)
            self._track_face(equalized_frame, colored_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):  # quit when 'q' keyword is pressed
                break
            raw_capture.truncate(0)

            if self.b_stop:
                break

        camera.close()
        cv2.destroyAllWindows()

    def _track_face(self, equalized_frame, cur_frame):
        try:
            # s_time = time.time()
            faces = detect_faces(equalized_frame)
            # if is_odroid():
            #     print 'Face detection time: {}'.format(time.time() - s_time)
            if len(faces) > 0:
                print 'Found {} face(s), current trackers: {}'.format(
                    len(faces), len([tr for tr in self.trackers if tr['enabled']]))

            for tr in self.trackers:
                if tr['enabled']:
                    # Compare current faces to the old images to get similar faces.
                    s = find_similar_face(src_frame=tr['face'], src_rect=None, dst_frame=equalized_frame,
                                          dst_faces=faces)
                    if s is not None:
                        # Update rect if new one is larger than the old one.
                        if get_size_of_rect(s) > get_size_of_rect(tr['pos']):
                            tr['pos'] = s
                            tr['face'] = equalized_frame[s.top():s.bottom(), s.left():s.right()]
                            tr['face_extended'] = get_extended_face(cur_frame, s)
                        tr['last_seen'] = time.time()
                        faces.remove(s)
                    else:
                        # Upload face of dead trackers
                        if time.time() - tr['last_seen'] > keep_dead:
                            data = copy.deepcopy(tr)
                            data.pop('enabled')
                            print '=' * 100
                            dwelling_time = round(
                                data['last_seen'] - data['start_time'] - data['disappeared_time'] + keep_dead, 2)
                            logger.info(
                                'A face is disappeared: {}, dwelling time: {} sec'.format(data['pos'], dwelling_time))
                            print '=' * 100
                            if dwelling_time > 0:
                                # Check if detected face is same with background
                                r = data['pos']
                                similarity = compare_2_images(data.get('face'),
                                                              equalized_frame[r.top():r.bottom(), r.left():r.right()])
                                logger.debug('Similarity: {}'.format(similarity))
                                if similarity['mse'] < 3000 and similarity['ssim'] > .5:
                                    logger.warning(
                                        '??? Seems like I detected a background image.. Sorry, ignoring... :)')
                                else:
                                    data['dwelling_time'] = dwelling_time
                                    self.q.put(data)
                            tr['enabled'] = False
                            tr.pop('pos')
                            tr.pop('start_time')
                            tr.pop('face')
                            tr.pop('face_extended')
                            tr.pop('last_seen')
                            tr.pop('disappeared_time')
                        else:
                            tr['disappeared_time'] += (time.time() - self.last_track_time)

            # Start trackers for the new detected faces
            for r in faces:
                try:
                    rest_tracker = [tr for tr in self.trackers if not tr['enabled']][0]
                    rest_tracker['enabled'] = True
                    rest_tracker['pos'] = r
                    rest_tracker['start_time'] = time.time()
                    rest_tracker['last_seen'] = time.time()
                    rest_tracker['disappeared_time'] = 0
                    rest_tracker['face'] = equalized_frame[r.top():r.bottom(), r.left():r.right()]
                    rest_tracker['face_extended'] = get_extended_face(cur_frame, r)
                except IndexError:
                    logger.warning('Oh, found a face, but there is no remaining tracker!')
                    break

            # if is_odroid() or is_debug():
            #     print 'Elapsed time: {}'.format(time.time() - s_time)

            self.last_track_time = time.time()
            # cur_trackers = [tr for tr in self.trackers if tr['enabled']]
            # if cur_trackers:
            #     print datetime.datetime.now(), ' Current Trackers: ', len(cur_trackers)

        except Exception as e:
            logger.exception('Critical error: {}'.format(e))


def upload_face(q):
    """
    Upload data to the server after detecting age & gender
    :return:
    """
    while True:
        if q.qsize() == 0:
            time.sleep(1)
            continue
        data = q.get()

        r = data['pos']
        file_name = '{start_time}__{dwelling_time}.png'.format(
            start_time=time.strftime('%Y_%m_%d_%H_%M_%S', time.localtime(data['start_time'])),
            dwelling_time=data['dwelling_time'])
        full_file_path = os.path.join(face_dir, file_name)

        data.update({
            'pos_left': r.left(),
            'pos_right': r.right(),
            'pos_top': r.top(),
            'pos_bottom': r.bottom(),
            'width': r.right() - r.left(),
            'height': r.bottom() - r.top(),
        })

        if data['last_seen'] < data['start_time'] + data['dwelling_time']:
            # Face was detected only one time in this case
            data['last_seen'] = data['start_time'] + data['dwelling_time']

        data['id'] = get_id()
        data['age'], data['age_confidence'] = detect_age(data['face_extended'])
        if data['age'] in ['(0, 2)', '(4, 6)']:
            logger.warning('Detected age is wrong, maybe cropped too large? Retrying...')
            cv2.imwrite(full_file_path, data.get('face'))
            data['age'], data['age_confidence'] = detect_age(data['face'])
        else:
            # Save image frame to a jpg image file.
            # print 'Saving to {}'.format(full_file_path)
            cv2.imwrite(full_file_path, data.get('face_extended'))

        data['gender'], data['gender_confidence'] = detect_gender(data['face_extended'])
        data['file_name'] = file_name

        data.pop('face')
        data.pop('face_extended')
        data.pop('disappeared_time')
        data.pop('pos')
        logger.debug('Uploading metadata: {}'.format(data))
        url = get_config('api', 'url_data', 'http://138.68.81.200/facialdata')
        if allow_upload():
            fp = open(full_file_path, 'rb')
            try:
                response = requests.post(url, files={'media': fp}, data=data)
                # response = requests.post(url, data=data)
                if response.status_code == 200:
                    logger.debug('=== Succeeded to upload')
                else:
                    logger.error(
                        'Failed to upload image file - {}, reason: {}'.format(response.status_code, response.reason))
            except Exception as e:
                logger.exception('Failed to upload data: {}'.format(e))
            os.remove(full_file_path)
            fp.close()
        del data


def check_remote_repo(*args):
    while True:
        logger.debug('Checking remote repo...')
        msg = check_git_commit()
        if 'Already up-to-date.' not in msg:
            logger.warning('Attention, remote repository was updated, now restarting me after pulling...')
            logger.warning(msg)
            f.stop()
            break
        time.sleep(5)
        # Kill ssh-agent
        proc = subprocess.Popen(["pkill", "-f", "ssh-agent"], stdout=subprocess.PIPE)
        proc.wait()
        time.sleep(60 * 60 * int(get_config('general', 'check_update', '1')))
    restart_me()


def check_remote_config(*args):
    while True:
        url = get_config('api', 'url_config', 'http://138.68.81.200/config/')
        url += get_id()
        try:
            r = requests.get(url)
            remote_config = r.json()
            is_updated = False
            for section in remote_config.keys():
                for option in remote_config[section].keys():
                    if get_config(section, option, '') != str(remote_config[section][option]):
                        is_updated = True
                        logger.warning(
                            'New config found: {} - {} - {}'.format(section, option, remote_config[section][option]))
                        break

            if is_updated:
                logger.debug('Checked remote config, url: {}'.format(url))
                logger.warning('Remote config: {}'.format(remote_config))
                for section in remote_config.keys():
                    for option in remote_config[section].keys():
                        set_config(section, option, remote_config[section][option])
                f.stop()
                restart_me()

        except Exception as e:
            logger.exception('An error occurred while parsing remote config file: {}'.format(e))
        time.sleep(60 * 60 * int(get_config('general', 'check_update', '1')))


def restart_me():
    """
    For some reason, webcam gets busy after restarting updated code.
    So we are rebooting Odroid itself... :(
    :return:
    """
    f.stop()
    time.sleep(5)
    logger.warning('^^^^^^^ Restarting myself... ^^^^^^^^')
    if is_odroid():
        os.system('reboot')


def check_provision():
    conf_file = '/etc/supervisor/conf.d/face_provision.conf'
    if not os.path.exists(conf_file):
        logger.warning('Provision script not found, copying and rebooting...')
        try:
            shutil.copy(os.path.join(cur_dir, 'conf', 'face_provision.conf'), conf_file)
        except Exception as e:
            logger.error('Failed to copy config file: {}'.format(e))
        if is_odroid():
            os.system('reboot')


if __name__ == '__main__':

    logger.debug('============================  Starting Face Detector   ====================================')

    if is_odroid():
        threading.Thread(target=check_provision).start()

    if not is_debug():
        threading.Thread(target=check_remote_config).start()
        threading.Thread(target=check_remote_repo).start()

    queue = Queue()

    multiprocessing.Process(target=upload_face, args=(queue,)).start()

    f = FaceDetector(q=queue)
    f.start()

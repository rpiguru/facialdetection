import glob
import os
import sys

import cv2
import numpy

if os.uname()[4] == 'aarch64':  # Is Odroid?
    sys.path.append('/home/odroid/caffe-rc5/python')

import caffe

gender_list = ['Male', 'Female']

model_dir = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'model', 'caffe', 'age_gender')

# Loading the mean image
mean_filename = os.path.join(model_dir, 'mean.binaryproto')
proto_data = open(mean_filename, "rb").read()
a = caffe.io.caffe_pb2.BlobProto.FromString(proto_data)
mean = caffe.io.blobproto_to_array(a)[0]

# Loading the gender network model
gender_net_pretrained = os.path.join(model_dir, 'gender_net.caffemodel')

# Loading the gender network weights
gender_net_model_file = os.path.join(model_dir, 'deploy_gender.prototxt')

# Configure the network with the model and the weights
gender_net = caffe.Classifier(gender_net_model_file, gender_net_pretrained,
                              mean=mean,
                              channel_swap=(2, 1, 0),
                              raw_scale=255,
                              image_dims=(256, 256))


def detect_gender(img):
    """
    Detect gender from the numpy array image
    :param img: numpy array
    :return:
    """

    face = cv2.resize(img, (256, 256))
    # Convert ndarray type form int to float32 (0.0 ~ 1.0)
    face = face.astype(numpy.float32) / 255.0

    # Gender prediction
    prediction = gender_net.predict([face])
    index = prediction[0].argmax()
    return gender_list[index], round(prediction[0][index] * 100, 4)


if __name__ == '__main__':

    import time

    for img_path in glob.glob('../sample/*.jpg'):
        s_time = time.time()
        image = cv2.imread(img_path)
        gender = detect_gender(image)
        print('{}: {}, elapsed: {}'.format(img_path, gender, time.time() - s_time))

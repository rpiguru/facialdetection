import ConfigParser
import os
import settings

config = ConfigParser.RawConfigParser()
config_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir, settings.config_file)


def get_config(section, option, default=None):
    """
    Get configuration from the external config file
    :param section:
    :param option:
    :param default:
    :return:
    """
    try:
        config.read(config_file)
        return config.get(section=section, option=option)
    except ConfigParser.NoSectionError:
        return default
    except ConfigParser.NoOptionError:
        return default


def set_config(section, option, value):
    """
    Set new configuration to the external config file
    :param section:
    :param option:
    :param value:
    :return:
    """
    try:
        config.read(config_file)
    except Exception as e:
        print('Failed to read config file: {}'.format(e))
        return False

    if section not in config.sections():
        config.add_section(section=section)

    config.set(section, option, value)
    try:
        with open(config_file, 'w') as configfile:
            config.write(configfile)
        return True
    except IOError as e:
        print('Failed to write new config to the file: {}'.format(e))
        return False

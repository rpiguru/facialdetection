echo "Starting ssh-agent"
eval "$(ssh-agent -s)"

chmod 400 /home/odroid/facialdetection/cert/deploy_ro_1*
echo "Adding ssh-key"
ssh-add /home/odroid/facialdetection/cert/deploy_ro_1

cd /home/odroid/facialdetection
git reset --hard

echo "Pulling remote repo"
git pull

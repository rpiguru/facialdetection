import errno
import os
import cv2
import datetime
import numpy as np
from skimage.measure import compare_ssim
from utils.config_util import get_config


def get_resolution():
    width = int(get_config('video', 'width', '640'))
    height = int(get_config('video', 'height', '480'))
    # print 'Camera resolution: {}x{}'.format(width, height)
    return width, height


def get_frame_rate():
    fr = int(get_config('video', 'frame_rate', '20'))
    print 'Frame rate: {}'.format(fr)
    return fr


def clahe(frame):
    """
    CLAHE: Contrast Limited Adaptive Histogram Equalization)
    :param frame:
    :return:
    """
    clahed = cv2.createCLAHE(clipLimit=3., tileGridSize=(8, 8))

    # convert from BGR to LAB color space
    lab = cv2.cvtColor(frame, cv2.COLOR_BGR2LAB)

    # split on 3 different channels
    l, a, b = cv2.split(lab)

    # apply CLAHE to the L-channel
    l2 = clahed.apply(l)

    # merge channels
    lab = cv2.merge((l2, a, b))

    # convert from LAB to BGR
    img2 = cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)

    return img2


def save_to_file(frame):
    from utils.common import get_image_file_to_save
    path = get_image_file_to_save()
    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    print datetime.datetime.now(),  ': Saving to file: {} ({})'.format(path, cv2.imwrite(path, frame))
    return True


def mse(image1, image2):
    # the 'Mean Squared Error' between the two images is the
    # sum of the squared difference between the two images;
    # NOTE: the two images must have the same dimension
    err = np.sum((image1.astype("float") - image2.astype("float")) ** 2)
    err /= float(image1.shape[0] * image1.shape[1])

    # return the MSE, the lower the error, the more "similar"
    # the two images are
    return err


def compare_2_images(frame1, frame2):
    """
    Compare 2 image frames with MSE(Mean Square Error) & SSM(Structural Similarity Measure)
    :param frame1:
    :param frame2:
    :return: True if both are same, vice versa
    """
    # convert the images to gray-scale
    frame1 = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
    frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

    cv2.imwrite('faces/1.png', frame1)
    cv2.imwrite('faces/2.png', frame2)

    m = mse(frame1, frame2)
    s = compare_ssim(frame1, frame2)

    return {'mse': m, 'ssim': s}


def _variance_of_laplacian(frame):
    """
        Compute the Laplacian of the image and then return the focus
        measure, which is simply the variance of the Laplacian
    :param frame:
    :return:
    """
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    return cv2.Laplacian(gray, cv2.CV_64F).var()


def is_blurry(frame, threshold=100):
    return _variance_of_laplacian(frame) < threshold


if __name__ == '__main__':

    import glob

    for img in glob.glob(os.path.expanduser('~/Pictures/faces/*.png')):
        print 'L-Value of {}: {}'.format(img, _variance_of_laplacian(cv2.imread(img)))
